# Multipass Ubuntu
These scripts and configs are ment to be used with Ubuntu Jammy in multipass. 

A warning to most users. This is mainly done on a windows 11 machine in 
powershell with multipass installed and using hyper-v for the virtual machines.

```powershell
# make shure you have a Hyper-V virtal switch called 'lab-network' first
multipass launch --network name=lab-network,mode=manual --name kealab --cpus 4 --memory 2G lts --cloud-init cloud-config.yml
multipass shell kealab

```