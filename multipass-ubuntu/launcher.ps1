# This script is used to create and destroy the kealab instance.
# Mainly for conveniece when testing the cloud-init script.
# Not actually neccecary for the multipass-ubuntu project.
# Useage: .\launcher.ps1 create
param([string]$method) 


if ($method -eq "create") 
{
    Write-Host "Creating kealab instance..."
    & multipass launch --network name=lab-network,mode=manual --name kealab --cpus 4 --memory 2G lts --cloud-init cloud-config.yml
    Write-Host "Waiting for cloud-init to finish..."
    #& multipass shell kealab -- cloud-init status --wait
    & multipass shell kealab
}

if ($method -eq "destroy") 
{
    Write-Host "Destroying kealab instance..."
    & multipass delete --purge kealab
}
