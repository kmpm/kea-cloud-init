# Kea-Cloud-Init
This is my work in progess for installing kea and stork using cloud-init.
To begin with using multipass and ubuntu as base but I would like to get
it to work on a debian lxc container in proxmox.




## Shoulders to stand on
- [isc kea packages](https://kb.isc.org/docs/isc-kea-packages)
- [how to setup kea repos from isc/cloudsmith](https://cloudsmith.io/~isc/repos/kea-2-5/setup/#formats-deb)
- [cloud-init and apt keys](https://stackoverflow.com/questions/72620609/cloud-init-fetch-apt-key-from-remote-file-instead-of-from-a-key-server)
- https://stackoverflow.com/questions/18691867/where-to-find-logs-for-a-cloud-init-user-data-script
- https://web-wilke.de/install-and-run-kea-dhcp-with-stork-on-debian-11/
- https://ubuntu.com/server/docs/how-to-install-and-configure-isc-kea


## Notes
- Cloud init output log can be checked at `/var/log/cloud-init-output.log`
